#!/usr/bin/env python3

from flask import Flask, jsonify, request
from lifxlan import LifxLAN

app = Flask(__name__)


lx = LifxLAN(2)
# get devices
print("Discovering lights...")
devices = lx.get_lights()

print("Found {} lights:".format(len(devices)))


def get_device_state(mac):
    for i in range(15):
        try:
            for device in devices:
                dmac = device.get_mac_addr()
                if dmac == mac:
                    h, s, l, kelvin = device.get_color()
                    return {
                        'name': device.get_label(),
                        'group': device.get_group_label(),
                        'color': {'h': h,
                                  's': s,
                                  'l': l,
                                  'k': kelvin},
                        'location': device.get_location(),
                        'power': device.get_power(),
                        'mac': device.get_mac_addr()}
        except Exception as e:
            print(e)
    return {'error': 'device not found'}


@app.route('/discover', methods=['GET'])
def discover():
    global devices
    devices = lx.get_lights()
    macs = []
    for device in devices:
        try:
            macs.append(get_device_state(device.get_mac_addr()))
        except Exception as e:
            print(e)
    return jsonify(macs)


@app.route('/light/<mac>', methods=['POST'])
def post_light(mac):
    for device in devices:
        try:
            dmac = device.get_mac_addr()
            if dmac == mac:
                # device found!

                transition_time_ms = 5000
                rapid = True if transition_time_ms < 1000 else False
                if 'color' in request.json:
                    c = request.json['color']
                    color = (int(c['h']), int(c['s']),
                             int(c['l']), int(c['k']))
                    for i in range(15):
                        try:
                            device.set_color(color, transition_time_ms, rapid)
                            break
                        except Exception as e:
                            print(e)

                if 'power' in request.json:
                    for i in range(15):
                        try:
                            device.set_power(int(request.form['power']))
                            break
                        except Exception as e:
                            print(e)

                # device.set_color
                return jsonify(get_device_state(mac))
        except Exception as e:
            print(e)
            return jsonify({'success': False, 'error': str(e)})


@app.route('/light/<mac>', methods=['GET'])
def get_light(mac):
    # Return the light.
    try:
        return jsonify(get_device_state(mac))
    except Exception as e:
        print(e)
        return jsonify({'error': str(e)})


if __name__ == '__main__':
    app.run(debug=True)
